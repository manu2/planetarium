//
//  GameViewController.swift
//  planetarium
//
//  Created by Emmanuel Orvain on 17/04/2019.
//  Copyright © 2019 Emmanuel Orvain. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = SCNScene(named:"art.scnassets/SolarSystem.scn")
        let view = self.view as! SCNView
        view.scene = scene
        
        let camera = scene?.rootNode.childNode(withName: "cameraSympa",
                                               recursively: true)
        view.pointOfView = camera
    }
    
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

}
